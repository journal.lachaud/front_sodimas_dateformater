import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {
  numberRoman = "";
  numberDec = "";
  constructor(private http: HttpClient) {

  }

  ngOnInit(): void {
  }


  getNumber(): void {
    this.http.get<any>('http://localhost:8000/arab-to-roman?date='+this.numberDec).subscribe(data => {
      this.numberRoman = data.romannumber;
    })
  }

}
